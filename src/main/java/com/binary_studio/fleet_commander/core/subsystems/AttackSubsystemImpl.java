package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger baseDamage;

	private final PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name.trim().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, baseDamage, optimalSize, optimalSpeed, capacitorConsumption,
				powergridRequirments);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {

		double sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
				: (target.getSize().value().doubleValue() / this.optimalSize.value());

		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: (this.optimalSpeed.value() / (2.0 * target.getCurrentSpeed().value()));
		double damage = this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);

		return PositiveInteger.of((int) Math.ceil(damage));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
