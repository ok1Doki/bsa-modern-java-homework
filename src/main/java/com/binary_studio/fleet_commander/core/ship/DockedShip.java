package com.binary_studio.fleet_commander.core.ship;

import java.util.ArrayList;
import java.util.List;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger capacitorRegeneration;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final PositiveInteger capacitor;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger pg;

	private List<DefenciveSubsystem> defenciveSubsystems;

	private List<AttackSubsystem> attackSubsystems;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
		this.defenciveSubsystems = new ArrayList<>();
		this.attackSubsystems = new ArrayList<>();
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystems.clear();
		}
		else if (checkAvailability(subsystem)) {
			this.attackSubsystems.add(subsystem);
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystems.clear();
		}
		else if (checkAvailability(subsystem)) {
			this.defenciveSubsystems.add(subsystem);
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {

		boolean hasDef = this.defenciveSubsystems.size() > 0;
		boolean hasOff = this.attackSubsystems.size() > 0;

		if (!hasDef & !hasOff) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (!hasDef) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (!hasOff) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else {
			return new CombatReadyShip(this);
		}
	}

	private <T extends Subsystem> boolean checkAvailability(T subsystem) throws InsufficientPowergridException {
		int sumPg = subsystem.getPowerGridConsumption().value();

		for (DefenciveSubsystem defenciveSubsystem : this.defenciveSubsystems) {
			sumPg += defenciveSubsystem.getPowerGridConsumption().value();
		}

		for (AttackSubsystem attackSubsystem : this.attackSubsystems) {
			sumPg += attackSubsystem.getPowerGridConsumption().value();
		}

		if (sumPg > this.pg.value()) {
			throw new InsufficientPowergridException(sumPg - this.pg.value());
		}

		return true;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getCapacitorRegeneration() {
		return this.capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return this.pg;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public PositiveInteger getCapacitor() {
		return this.capacitor;
	}

	public List<DefenciveSubsystem> getDefenciveSubsystems() {
		return this.defenciveSubsystems;
	}

	public List<AttackSubsystem> getAttackSubsystems() {
		return this.attackSubsystems;
	}

}
