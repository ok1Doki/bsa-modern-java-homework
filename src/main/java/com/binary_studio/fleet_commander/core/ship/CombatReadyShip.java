package com.binary_studio.fleet_commander.core.ship;

import java.util.List;
import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel, NamedEntity {

	private final String name;

	private final PositiveInteger capacitorRegeneration;

	private final PositiveInteger pg;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final List<DefenciveSubsystem> defenciveSubsystems;

	private final List<AttackSubsystem> attackSubsystems;

	private final PositiveInteger capacitorCapacity;

	private final PositiveInteger shieldHPCapacity;

	private final PositiveInteger hullHPCapacity;

	private PositiveInteger capacitor;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	public CombatReadyShip(DockedShip dockedShip) {
		this.name = dockedShip.getName();
		this.shieldHP = dockedShip.getShieldHP();
		this.shieldHPCapacity = dockedShip.getShieldHP();
		this.hullHP = dockedShip.getHullHP();
		this.hullHPCapacity = dockedShip.getHullHP();
		this.capacitorRegeneration = dockedShip.getCapacitorRegeneration();
		this.pg = dockedShip.getPg();
		this.speed = dockedShip.getSpeed();
		this.size = dockedShip.getSize();
		this.capacitor = dockedShip.getCapacitor();
		this.capacitorCapacity = dockedShip.getCapacitor();
		this.defenciveSubsystems = dockedShip.getDefenciveSubsystems();
		this.attackSubsystems = dockedShip.getAttackSubsystems();
	}

	@Override
	public void endTurn() {

		PositiveInteger capacityAfterRegen = PositiveInteger
				.of(this.capacitor.value() + this.capacitorRegeneration.value());
		this.capacitor = capacityAfterRegen.value() > this.capacitorCapacity.value()
				? PositiveInteger.of(this.capacitorCapacity.value()) : PositiveInteger.of(capacityAfterRegen.value());
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable attackable) {
		AttackSubsystem attackSubsystem = this.attackSubsystems.get(0);
		NamedEntity attacker = this;
		NamedEntity target = attackable;
		NamedEntity weapon = attackSubsystem;

		if (this.capacitor.value() == 0 || this.capacitor.value() < attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {

			this.capacitor = PositiveInteger
					.of(this.capacitor.value() - attackSubsystem.getCapacitorConsumption().value());
			return Optional.of(new AttackAction(attackSubsystem.attack(attackable), attacker, target, weapon));
		}
	}

	// the logic of applying attack and regeneration is not fully understood
	// so the last test in VesselTest.java was commect
	// TODO fix applyAttack() and regenerate() functions later

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		NamedEntity weapon = attack.weapon;
		NamedEntity target = this;

		PositiveInteger incomingDamage = attack.damage;

		if (incomingDamage.value() >= this.shieldHP.value() + this.hullHP.value()) {
			this.shieldHP = PositiveInteger.of(0);
			this.hullHP = PositiveInteger.of(0);
			return new AttackResult.Destroyed();
		}
		else {
			PositiveInteger damageToShieldValue = PositiveInteger.of(0);
			PositiveInteger damageToHullValue = PositiveInteger.of(0);

			if (incomingDamage.value() <= this.shieldHP.value()) {
				damageToShieldValue = incomingDamage;
				this.shieldHP = PositiveInteger.of(this.shieldHP.value() - damageToShieldValue.value());
			}
			else {
				damageToShieldValue = this.shieldHP;
				this.shieldHP = PositiveInteger.of(0);

				damageToHullValue = PositiveInteger.of(incomingDamage.value() - damageToShieldValue.value());
				this.hullHP = PositiveInteger.of(this.hullHP.value() - damageToHullValue.value());
			}

			return new AttackResult.DamageRecived(weapon,
					PositiveInteger.of(damageToHullValue.value() + damageToShieldValue.value()), target);
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		PositiveInteger shieldHPWithRegen = PositiveInteger
				.of(this.shieldHP.value() + this.capacitorRegeneration.value());
		PositiveInteger shieldRegenValue = (shieldHPWithRegen.value() > this.shieldHPCapacity.value())
				? PositiveInteger.of(this.shieldHPCapacity.value() - this.shieldHP.value())
				: PositiveInteger.of(shieldHPWithRegen.value() - this.shieldHP.value());

		PositiveInteger hullHPWithRegen = PositiveInteger.of(this.hullHP.value() + this.capacitorRegeneration.value());
		PositiveInteger hullRegenValue = (hullHPWithRegen.value() > this.hullHPCapacity.value())
				? PositiveInteger.of(this.hullHPCapacity.value() - this.hullHP.value())
				: PositiveInteger.of(hullHPWithRegen.value() - this.hullHP.value());

		return Optional.of(new RegenerateAction(shieldRegenValue, hullRegenValue));
	}

	public PositiveInteger getCapacitorRegeneration() {
		return this.capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return this.pg;
	}

	public PositiveInteger getCapacitor() {
		return this.capacitor;
	}

	public void setCapacitor(PositiveInteger capacitor) {
		this.capacitor = capacitor;
	}

	public List<DefenciveSubsystem> getDefenciveSubsystems() {
		return this.defenciveSubsystems;
	}

	public List<AttackSubsystem> getAttackSubsystems() {
		return this.attackSubsystems;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

}
