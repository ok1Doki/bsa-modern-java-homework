package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		int max = 0;
		for (Department child : rootDepartment.subDepartments) {
			max = Math.max(calculateMaxDepth(child), max);
		}
		return 1 + max;
	}

}
